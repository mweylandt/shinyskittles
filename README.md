# shinySkittles

A shiny app that spits out a number of different-coloured skittles, made for teaching CLT. 

Viewable on https://maxweylandt.shinyapps.io/shinyskittles

When I teach the sampling distribution, I like to give everyone a mini packet of skittles and collectively draw up a distribution of proportions of green skittles. Covid made this untenable, and so for students who forgot to bring their own skittles to class I made a shiny app that would simulate a draw for them. 

This could be extended quite easily to make it more useful for continued use in remote environments, e.g. showing proportion after a user picks the color, changing the N, drawing a histogram after repeated samples. But this was never meant to be an instructional app by itself, just a tool for my process, so it was fine as is. 
